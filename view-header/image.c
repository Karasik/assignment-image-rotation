#include <stdio.h>
#include <malloc.h>
#include <inttypes.h>
#include "image.h"
void alloc_img(struct image* img){
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
}



void free_img(struct image* img){
    free(img->data);
}