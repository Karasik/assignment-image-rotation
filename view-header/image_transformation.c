#include "image_transformation.h"
#include <inttypes.h>

struct image img_rotation(struct image const img ){
    struct image* img_rotated = &((struct image) {.width = img.height, .height = img.width, 0});
    alloc_img(img_rotated);
    for(size_t i = 0;
        i < img_rotated->height;
        i++){
        for(size_t j = 0;
                j < img_rotated->width;
                j++){
            img_rotated->data[i*img_rotated->width + j] = 
                    img.data[(img.height - j - 1)*img.width + i];
        }
    }
    return *img_rotated;
}
