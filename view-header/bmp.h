#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdint.h>
#include "image.h"

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_STREAM_ERROR
    };
    
enum read_status from_bmp_file(const char* filename, struct image* img);

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_STREAM_ERROR,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_BITS
    /* коды других ошибок  */
};

enum write_status to_bmp_file( const char* filename, struct image* img);
#endif