#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;

struct __attribute__((packed)) bmp_header 
{
   FOR_BMP_HEADER( DECLARE_FIELD )
};

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");


#define PADDING 4

#define BM 19778
#define HEADER_SIZE 54
#define DIB_HEADER_SIZE 40
#define PLANES 1
#define BITS_PER_PIXEL 24


static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

static uint32_t calculate_image_size(struct image* img){
    return img->height * (sizeof(struct pixel) * img->width + img->width % PADDING);
}

static struct bmp_header generate_header(struct image* img){
    struct bmp_header header = (struct bmp_header){0};
    header.bfType = BM;
    header.bfileSize = HEADER_SIZE + calculate_image_size(img);
    header.bOffBits = HEADER_SIZE;
    header.biSize = DIB_HEADER_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = PLANES;
    header.biBitCount = BITS_PER_PIXEL;
    header.biSizeImage = calculate_image_size(img);
    return header;
}

uint64_t fill_img_from_stream(FILE* in, const uint32_t offset, const struct image* img){
    fseek(in, offset, SEEK_SET);
    size_t pixel_count = 0;
    for(size_t i = 0;
            i < img->width * img->height;
            i += img->width){
        pixel_count += fread(&(img->data[i]), sizeof(struct pixel), img->width, in);
        fseek(in, img->width % PADDING, SEEK_CUR);
    }
    return pixel_count;
}

uint64_t fill_stream_from_img(FILE* out, const uint32_t offset, const struct image* img){
    fseek(out, offset, SEEK_SET);
    size_t pixel_count = 0;
    uint8_t padder[PADDING] = {0};
    for(size_t i = 0;
            i < img->width * img->height;
            i += img->width){
        pixel_count += fwrite(&(img->data[i]), sizeof(struct pixel), img->width, out);
        fwrite(&padder, sizeof(uint8_t), img->width % PADDING, out);
    }
    return pixel_count;
}

static enum read_status from_bmp( FILE* in, struct image* img, struct bmp_header header){
    if(!read_header(in, &header)) return READ_INVALID_HEADER;
    img->height = header.biHeight;
    img->width = header.biWidth;
    alloc_img(img);
    if(fill_img_from_stream(in, header.bOffBits, img) != img->width * img->height) 
        return READ_INVALID_BITS;
    return READ_OK;
}

enum read_status from_bmp_file(const char* filename, struct image* img){
    if (!filename) return READ_INVALID_SIGNATURE;
    FILE* in = fopen(filename, "rb");
    if(!in) return READ_STREAM_ERROR;
    enum read_status status = from_bmp(in, img, (struct bmp_header){0});
    fclose(in);
    return status;
}

static enum write_status to_bmp( FILE* out, struct image* img, const struct bmp_header header ){
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out))
        return WRITE_INVALID_HEADER;
    fseek(out, header.bOffBits, SEEK_SET);
    if(fill_stream_from_img(out, header.bOffBits, img) != img->width * img->height) 
        return WRITE_INVALID_BITS;
    return WRITE_OK;
}

enum write_status to_bmp_file(const char* filename, struct image* img){
    FILE* out = fopen(filename, "wb");
    if(!out) return WRITE_STREAM_ERROR;
    enum write_status status = to_bmp(out, img, generate_header(img));
    fclose(out);
    return status;
}