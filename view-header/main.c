#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#include "bmp.h"
#include "util.h"
#include "image.h"
#include "image_transformation.h"


static char* read_status_prints[] = {
    [READ_OK] = "Good",
    [READ_INVALID_SIGNATURE] = "The file you have entered is not to be found",
    [READ_INVALID_BITS] = "This pixels in this file are not represented with 24 bits per pixel",
    [READ_INVALID_HEADER] = "The header of this file is corrupted",
    [READ_STREAM_ERROR] = "The stream for reading seams off."
}; 

static char* write_status_prints[] = {
    [WRITE_OK] = "Good",
    [WRITE_STREAM_ERROR] = "The stream for writing seems off",
    [WRITE_INVALID_HEADER] = "The header is invalid",
    [WRITE_INVALID_BITS] = "This file is corrupted"
};

void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}

int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );
    
    struct image* img = &((struct image) {0});
    enum read_status reading = from_bmp_file(argv[1], img);
    if(reading){
        free_img(img);
        err("%s\n", read_status_prints[reading]);
    }
    struct image img_rotated = img_rotation(*img);
    free_img(img);
    enum write_status writing = to_bmp_file("rotated.bmp", &img_rotated);
    if(writing){
        free_img(&img_rotated);
        err("%s\n", write_status_prints[writing]);
    }
    free_img(&img_rotated);
    return 0;
}
