#ifndef IMAGE_H
#define IMAGE_H
#include <stdio.h>
#include <stdint.h>

struct pixel { uint8_t b, g, r; };
struct image {
  uint64_t width, height;
  struct pixel* data;
};
void alloc_img(struct image* img);
void free_img(struct image* img);
#endif 

